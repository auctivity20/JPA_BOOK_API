package com.auctivity.db.JPA_Hibernate;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.auctivity.db.JPA_Hibernate.http.BooksServiceResource;
import com.auctivity.db.JPA_Hibernate.models.Book;
import com.auctivity.db.JPA_Hibernate.service.BookService;


@RunWith(SpringRunner.class)
@SpringBootTest
class BookServiceDBUnitTest {

	@Autowired
    private BookService bookService;
	@Autowired
	private BooksServiceResource booksResource;
	
	@Test //Any change to the DB may result in this test failing (affected by the test below adding a book)
	void testWhenApplicationStarts_thenH2Contains3Entries() {
	List<Book> books = bookService.list();

    Assertions.assertEquals(4, books.size());
    
	}
	
	@Test
	void testNewBooksServiceResourceCanAddBookAtRuntime() {
		
		Book newTitle = new Book ("The boys in the hood");
		booksResource.addBook(newTitle);
		List<Book> books = bookService.list();
		Assertions.assertEquals(4, books.size());
	}

}
