package com.auctivity.db.JPA_Hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("deprecation")
@ComponentScan({ "com.auctivity.db.JPA_Hibernate", "com.auctivity.db.JPA_Hibernate.models",
		"com.auctivity.db.JPA_Hibernate.repository", "com.auctivity.db.JPA_Hibernate.service",
		"com.auctivity.db.JPA_Hibernate.http", "com.auctivity.db.JPA_Hibernate.config" })
@SpringBootApplication(scanBasePackages = { "com.auctivity.db.JPA_Hibernate", "com.auctivity.db.JPA_Hibernate.models",
		"com.auctivity.db.JPA_Hibernate.repository", "com.auctivity.db.JPA_Hibernate.service",
		"com.auctivity.db.JPA_Hibernate.http", "com.auctivity.db.JPA_Hibernate.config" })
@EnableWebMvc
public class JpaHibernateApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(JpaHibernateApplication.class, args);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("swagger-ui.html")
	      .addResourceLocations("classpath:/META-INF/resources/");

	    registry.addResourceHandler("/webjars/**")
	      .addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
}
