package com.auctivity.db.JPA_Hibernate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//TODO: import io.swagger.annotations.ApiModelProperty;


@Table(name = "book")
@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) //strategy=GenerationType.AUTO / 
	private Long id;
	private String name;
	
	protected Book() {}
	
	public Book(String name) {
		this.id = 10L;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + "]";
	}
	
}
