package com.auctivity.db.JPA_Hibernate.http;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auctivity.db.JPA_Hibernate.models.Book;
import com.auctivity.db.JPA_Hibernate.repository.BookRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Books endoints")
@RestController
@RequestMapping(value = { "/api/v1/books/" })
public class BooksServiceResource {

	@Autowired(required = true)
	private HttpServletRequest incomingServletRequest;
	@Autowired(required = true)
	private HttpServletResponse incomingServletResponse;
	@Autowired
	private BookRepository bookRepo;

	public BooksServiceResource() {
		
	}
	
	public BooksServiceResource(HttpServletRequest request) {
		this.incomingServletRequest = request;
	}

	/**
	 * @GetBookById
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation("Find a book by known id")
	@RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
	public @ResponseBody Optional<Book> getBookById(HttpServletRequest request,
			@RequestParam(required = false, value = "id") String id) {

		// Parsing the URI string on /book/ to get {id}
		String[] arrOfStr = request.getRequestURI().split("/book/");
		// Give precedence to RequestParam if present and disregard URI
		String searchWith = id != null ? id : arrOfStr[1];

		if (searchWith != null && searchWith.matches("[0-9]+") && Integer.parseInt(searchWith) >= 1) {
			Long searchParam = Long.parseLong(searchWith);

					return bookRepo.findById(searchParam);
		}

		Book demoBook = new Book("The Boys In The Hood");
		return Optional.ofNullable(demoBook);

	}

	/**
	 * @getBookByName
	 * 
	 * @param bookName
	 * @return
	 */
	@ApiOperation("Find a book by title")
	@GetMapping("/one/**")
	public @ResponseBody Optional<Book> getBookByName(HttpServletRequest request, String bookName) {

		// Parsing the URI string on "/book/" to get {title}
		String[] arrOfStr = request.getRequestURI().split("/one/");
		Optional<Book> demoBook = Optional.of(new Book("The Boys In The Hood"));
		
		String searchWith = "";
		String[] arrOfUri = new String[100];
		// Give precedence to RequestParam if present and disregard URI
		if (arrOfStr.length > 1) {
			searchWith = bookName != null ? bookName : arrOfStr[1];
			arrOfUri = arrOfStr[1].split("%20");
		} else {
			return demoBook;
		}
		
		int i = 1;
		String searchParam = "";

		while (i <= arrOfUri.length && arrOfUri.length > 0) {
			searchParam = searchParam + " " + arrOfUri[i - 1];
			i = i + 1;
		}

		if (searchWith != null) {
			if (!bookRepo.findByName(searchParam.trim()).isPresent()) {
				return demoBook;
			} else {
				return bookRepo.findByName(searchParam.trim());
			}
		}

		return demoBook;
	}

	/**
	 * @fetchBooks
	 * 
	 * @return
	 */
	@ApiOperation("Find all books")
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<Book> fetchBooks(HttpServletRequest request) {
		return bookRepo.findAll();

	}
	
	/**
	 * @updateBook
	 * 
	 * @param id
	 * @param book
	 * @return
	 */
	@PutMapping("/book/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable("id") long id, @RequestBody Book book) {
		Optional<Book> bookData = bookRepo.findById(id);
		if (bookData.isPresent()) {
			Book _book = bookData.get();
			_book.setName(book.getName());
			return new ResponseEntity<>(bookRepo.save(_book), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	/**
	 * @addBook
	 * 
	 * @param book
	 * @return
	 */
	@PostMapping("/book/add")
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		System.out.println("*** Book is ("+ book.getId()+", "+ book.getName()+")");
		try {
			Book _book = bookRepo
					.save(new Book(book.getName()));
			return new ResponseEntity<>(_book, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("Error is " + e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	 
}
