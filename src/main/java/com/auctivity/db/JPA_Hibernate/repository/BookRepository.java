package com.auctivity.db.JPA_Hibernate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.auctivity.db.JPA_Hibernate.models.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

	Optional<Book> findByName(String name);
	Optional<Book> findById(Long id);
	List<Book> findAll();
	

}
