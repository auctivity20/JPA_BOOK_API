package com.auctivity.db.JPA_Hibernate.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auctivity.db.JPA_Hibernate.models.Book;
import com.auctivity.db.JPA_Hibernate.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;

	public List<Book> list() {
		return (List<Book>) bookRepository.findAll();
	}
	
}
